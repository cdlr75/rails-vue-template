#!/bin/bash
set -e

rm -f /usr/src/app/tmp/pids/server.pid

wait-for-it psql:5432
wait-for-it redis:6379

if [ "$SERVER_DB_CREATE" == "true" ]; then
    echo "rails db:create"
    rails db:create
fi

if [ "$SERVER_DB_MIGRATE" == "true" ]; then
    echo "rails db:migrate"
    rails db:migrate
fi

if [ "$SERVER_DB_SEED" == "true" ]; then
    echo "rails db:seed"
    rails db:seed
fi

exec "$@"
