FROM ruby:3.0.2

ARG install="bundle install --jobs=4"

RUN apt update -y && \
    apt install -y nodejs npm libpq-dev wait-for-it libsodium-dev && \
    npm install --global yarn

WORKDIR /usr/src/app

COPY app/Gemfile app/Gemfile.lock ./

RUN gem install bundler

RUN echo "$install"
RUN eval $install

COPY app .

RUN bundle exec rails assets:precompile
RUN bundle exec rails webpacker:compile

EXPOSE 3035
EXPOSE 8080

ENTRYPOINT ["/usr/src/app/entrypoint.sh"]

CMD ["rails", "s", "-b", "0.0.0.0", "-p", "8080"]
